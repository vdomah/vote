$(function(){
    $(".jRate").each(function(){
        var id = $(this).attr('id');
        $('#'+id).jRate({
          rating:$(this).data('rating'),
          onSet: function(rating) {
            $.post('/rating',{rating:rating, id:id},function(data){
                $('#rating'+data.id).text(data.rating);
            },'json');
          }
        });
    });
});