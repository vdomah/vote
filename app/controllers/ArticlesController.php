<?php
/**
 * Created by PhpStorm.
 * User: Nikolai Lisniak
 * Date: 04.03.2015
 * Time: 0:30
 */

//use Artvenue\Repository\ArticlesRepositoryInterface;

class ArticlesController extends BaseController {

    /**
     * @param ArticlesRepositoryInterface $articles
     */
    public function __construct()
    {
        //$this->articles = $articles;
    }

    public function getIndex()
    {
        $articles = Article::all();
        $title = t('Latest Articles');

        return View::make('articles/index', compact('articles', 'title'));
    }
}