<?php
use Artvenue\Validator\ReportValidator;

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
class ReportController extends BaseController {

    /**
     * @param ReportValidator $validator
     */
    public function __construct(ReportValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function postReportUser($username)
    {
        if ( ! $this->validator->validCreate(Input::all()))
        {
            return Redirect::back()->withErrors($this->validator->errors());
        }
        $report = new Report();
        $report->report = $username;
        $report->type = 'user';
        $report->user_id = Auth::user()->id;
        $report->description = Input::get('report');
        $report->save();

        return Redirect::route('gallery')->with('flashSuccess', 'Thanks, user is now reported we will take quick actions');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postReportImage($id)
    {
        if ( ! $this->validator->validCreate(Input::all()))
        {
            return Redirect::back()->withErrors($this->validator->errors());
        }

        $report = new Report();
        $report->report = $id;
        $report->user_id = Auth::user()->id;
        $report->type = 'image';
        $report->description = Input::get('report');
        $report->save();

        return Redirect::route('gallery')->with('flashSuccess', 'Thanks, Image is now reported we will take quick actions');
    }

    /**
     * @return mixed
     */
    public function getReport()
    {
        $title = t('Report');

        return View::make('report/index', compact('title'));
    }
}