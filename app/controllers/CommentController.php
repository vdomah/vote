<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Artvenue\Repository\CommentsRepositoryInterface;
use Artvenue\Validator\CommentValidator;

class CommentController extends BaseController {

    /**
     * @param CommentValidator            $validator
     * @param CommentsRepositoryInterface $comments
     */
    public function __construct(CommentValidator $validator, CommentsRepositoryInterface $comments)
    {
        $this->validator = $validator;
        $this->comments = $comments;
    }

    /**
     * @return string
     */
    public function postDeleteComment()
    {
        // If comment delete success then return success
        if ($this->comments->delete(Input::get('id')))
        {
            return 'success';
        }

        return 'failed';
    }

    /**
     * @param $id
     * @param $slug
     * @return mixed
     */
    public function postComment($id, $slug)
    {
        if ( ! $this->validator->validComment(Input::all()))
        {
            return Redirect::route('image', ['id' => $id, 'slug' => $slug])->withErrors($this->validator->errors());
        }

        if ( ! $this->comments->create($id, Input::all()))
        {
            return Redirect::to('gallery')->with('flashError', t('You are not allowed'));
        }

        return Redirect::route('image', ['id' => $id, 'slug' => $slug])->with('flashSuccess', t('Your comment is added'));
    }
}