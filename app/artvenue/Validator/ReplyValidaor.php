<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */

namespace Artvenue\Validator;

use Reply;

class ReplyValidator extends Validator {

    protected $replyRules = [
        'reply_msgid' => ['required'],
        'textcontent' => ['required']
    ];

    public function __construct(Reply $model)
    {
        $this->model = $model;
    }
}