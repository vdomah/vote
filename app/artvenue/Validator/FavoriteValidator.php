<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Artvenue\Validator;

use Favorite;

class FavoriteValidator extends Validator {

    protected $favoriteRules = [
        'id' => ['required', 'integer']
    ];


    public function __construct(Favorite $model)
    {
        $this->model = $model;
    }

}