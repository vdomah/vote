<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */

namespace Artvenue\Validator;

use Comment;

class CommentValidator extends Validator {

    protected $commentRules = [
        'comment' => ['required', 'min:2']
    ];

    public function __construct(Comment $model)
    {
        $this->model = $model;
    }
}