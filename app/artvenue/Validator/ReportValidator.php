<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */

namespace Artvenue\Validator;

use Reply;

class ReportValidator extends Validator {

    protected $createRules = [
        'report'               => 'required|min:10|max:200',
        'g-recaptcha-response' => ['required', 'captcha']
    ];

    public function __construct(Reply $model)
    {
        $this->model = $model;
    }
}