<?php
/**
 * @author Nikolai Lisniak
 */
namespace Artvenue\Repository;

use Auth;
use Cache;
use DB;
use File;
use Str;

interface ArticlesRepositoryInterface {

    /**
     * @return mixed
     */
    public function getAll();
}