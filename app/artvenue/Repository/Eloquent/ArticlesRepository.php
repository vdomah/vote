<?php

namespace Artvenue\Repository\Eloquent;

use Artvenue\Repository\ArticlesRepositoryInterface;
use Auth;
use Cache;
use DB;
use File;
use Article;
use Str;

class ArticlesRepository extends AbstractRepository implements ArticlesRepositoryInterface {

    public function __construct(Article $articles)
    {
        $this->article = $articles;
    }

    public function getAll(){
        return Article::all();
    }
}