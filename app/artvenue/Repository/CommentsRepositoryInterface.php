<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Artvenue\Repository;

use Auth;

interface CommentsRepositoryInterface {

    /**
     * @param $id
     * @param $input
     * @return mixed
     */
    public function create($id, $input);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

}