<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Artvenue\Repository;


interface BlogsRepositoryInterface {

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param null $paginate
     * @return mixed
     */
    public function getLatestBlogs($paginate = null);

}