<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Artvenue\Repository;

use Auth;

interface ReplyRepositoryInterface {

    /**
     * @param $input
     * @return mixed
     */
    public function createReply($input);

    /**
     * @param $input
     * @return mixed
     */
    public function deleteReply($input);
}