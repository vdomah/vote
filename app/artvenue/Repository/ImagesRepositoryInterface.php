<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Artvenue\Repository;

use Auth;
use Cache;
use DB;
use File;
use Images;
use Str;

interface ImagesRepositoryInterfaceInterface {

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);


    /**
     * @param null $category
     * @param null $timeframe
     * @return mixed
     */
    public function getLatest($category = null, $timeframe = null);


    /**
     * @param null $category
     * @param null $timeframe
     * @return mixed
     */
    public function getFeatured($category = null, $timeframe = null);


    /**
     * @param array $input
     * @param       $id
     * @return mixed
     */
    public function update(array $input, $id);


    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);


    /**
     * @param      $tag
     * @return mixed
     */
    public function getByTags($tag);


    /**
     * @param $image
     * @return mixed
     */
    public function incrementViews($image);


    /**
     * @param null $category
     * @param null $timeframe
     * @return mixed
     */
    public function mostCommented($category = null, $timeframe = null);


    /**
     * @param null $category
     * @return mixed
     */
    public function popular($category = null, $timeframe = null);


    /**
     * @param null $category
     * @param null $timeframe
     * @return mixed
     */
    public function mostFavorited($category = null, $timeframe = null);


    /**
     * @param null $category
     * @param null $timeframe
     * @return mixed
     */
    public function mostDownloaded($category = null, $timeframe = null);


    /**
     * @param null $category
     * @param null $timeframe
     * @return mixed
     */
    public function mostViewed($category = null, $timeframe = null);


    /**
     * @param      $input
     * @param null $category
     * @return mixed
     */
    public function search($input, $category = null);

    /**
     * @param Images $image
     * @return mixed
     */
    public function findNextImage(Images $image);


    /**
     * @param Images $image
     * @return mixed
     */
    public function findPreviousImage(Images $image);

    /**
     * @return mixed
     */
    public function postFavorite();

}