<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Artvenue\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Artvenue\Repository\ImagesRepositoryInterfaceInterface',
            'Artvenue\Repository\Eloquent\ImagesRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\UsersRepositoryInterface',
            'Artvenue\Repository\Eloquent\UsersRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\BlogsRepositoryInterface',
            'Artvenue\Repository\Eloquent\BlogsRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\CategoryRepositoryInterface',
            'Artvenue\Repository\Eloquent\CategoryRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\FlagsRepositoryInterface',
            'Artvenue\Repository\Eloquent\FlagsRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\CommentsRepositoryInterface',
            'Artvenue\Repository\Eloquent\CommentsRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\ReplyRepositoryInterface',
            'Artvenue\Repository\Eloquent\ReplyRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\VotesRepositoryInterface',
            'Artvenue\Repository\Eloquent\VotesRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\FollowRepositoryInterface',
            'Artvenue\Repository\Eloquent\FollowRepository'
        );

        $this->app->bind(
            'Artvenue\Repository\FavoriteRepositoryInterface',
            'Artvenue\Repository\Eloquent\FavoriteRepository'
        );
    }
}