<?php
/**
 * Created by PhpStorm.
 * User: Nikolai Lisniak
 * Date: 03.03.2015
 * Time: 23:56
 */

class Article extends Eloquent {
    /**
     * @var string
     */
    protected $table = 'articles';

    public function user()
    {
        return $this->belongsTo('Users');
    }
}