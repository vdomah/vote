<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Report extends Eloquent {

    use SoftDeletingTrait;
    
    /**
     * @var string
     */
    protected $table = 'report';
    /**
     * @var bool
     */
    protected $softDelete = true;

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User');
    }
}