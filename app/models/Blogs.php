<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
class Blogs extends Eloquent {

    protected $table = 'blogs';

    public function user()
    {
        return $this->belongsTo('User');
    }
}