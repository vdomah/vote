<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Images extends Eloquent {

    use SoftDeletingTrait;
    /**
     * @var string
     */
    protected $table = 'images';
    /**
     * @var bool
     */
    protected $softDelete = true;

    /**
     * @return mixed
     */
    public static function scopeApproved()
    {
        return static::whereNotNull('approved_at');
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * @return mixed
     */
    public function comments()
    {
        return $this->hasMany('Comment', 'image_id');
    }

    /**
     * @return mixed
     */
    public function favorite()
    {
        return $this->hasMany('Favorite', 'image_id');
    }

    /**
     * @return mixed
     */
    public function category()
    {
        return $this->belongsTo('Category', 'category_id');
    }

    /**
     * @return mixed
     */
    public function info()
    {
        return $this->hasOne('ImageInfo', 'image_id');
    }
    
    public function userRatings()
    {
    	return $this->belongsToMany('User', 'image_user_rating', 'image_id', 'user_id')->withPivot('rating');
    }
    
    public function getRatingAttribute()
    {
    	return number_format(DB::table('image_user_rating')->where('image_id','=',$this->id)->avg('rating'),1);
    }
    
    public function getVotesAttribute()
    {
    	return DB::table('image_user_rating')->where('image_id','=',$this->id)->groupBy('user_id')->count();
    }
}