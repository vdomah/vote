@extends('master/index')

@section('content')
    <h3 class="content-heading">{{ t('Password Reset') }}</h3>

    <h2>Registration</h2>
    <hr>
    {{ Form::open() }}
    <div class="form-group">
        <label for="email">Email
            <small>*</small>
        </label>
        {{ Form::text('email','',['class'=>'form-control','id'=>'email','placeholder'=>'Your Email','required'=>'required']) }}
    </div>
    <div class="form-group">
        <label for="recaptcha">Type these words
            <small>*</small>
        </label>
        {{ app('captcha')->display() }}
    </div>

    {{ Form::submit('Reset Password',['class'=>'btn btn-success'])}}
    {{ Form::close() }}
@stop