@extends('master/index')
@section('content')
    <h1 class="content-heading">{{ $title }}</h1>

    {{ Form::open() }}
    <div class="form-group">
        <label for="report">Reason For Reporting</label>
        {{ Form::textarea('report','',['class'=>'form-control','id'=>'username','placeholder'=>'Enter Some Details'])}}
    </div>
    <div class="form-group">
        {{ Form::submit('Report',['class'=>'btn btn-default'])}}
    </div>
    {{ Form::close() }}
@stop